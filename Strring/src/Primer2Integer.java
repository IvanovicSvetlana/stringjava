

/*
 * int or short number between -128 and 127 (inclusive) cache values
 */
public class Primer2Integer {
	public static void main(String[] args) {
		Integer i1 = 127;
		Integer i2 = 127;
		System.out.println(i1==i2); 
		
		Integer ii1 = new Integer(127);
		Integer ii2 = new Integer(127);
		System.out.println(ii1==ii2); // False
		
		Integer iii1 = 128;
		Integer iii2 = 128;
		System.out.println(iii1==iii2);
	}

}
