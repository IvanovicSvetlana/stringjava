
public class KvadratHipotenuze {
	static double hipotenuzaDvaParametra(double a, double b) {
		double c = Math.sqrt(a * a + b * b);
		return c;
	}

	// ako su katete istih velicina , treba nam samo jedan parametar
	static double hipotenuzaTrouglaJedanParametar(double a) {
		double c = Math.sqrt(a * a + a * a);
		return c;
	}

	public static void main(String[] args) {
double vrednost =0;
vrednost=hipotenuzaDvaParametra(2, 3);
System.out.println("Vrednost je "   + vrednost);
vrednost=hipotenuzaTrouglaJedanParametar(3);
System.out.println("Vrednost je  " +vrednost);
	}

}
